# Stage 1: Development and Builder
FROM node:19-alpine as development
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .

# Stage 2: Production
FROM node:19-alpine as production
WORKDIR /app
COPY --from=development /app/node_modules ./node_modules
COPY --from=development /app .
CMD ["npm", "run", "start"]
