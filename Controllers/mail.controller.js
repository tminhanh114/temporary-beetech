
const mailer = require('../Middlewares/mailer')

module.exports.send_mail = async (req, res) => {

    const data = req.body;

    //B3: Bắt đầu gửi Mail xác nhận đơn hàng
    const htmlHead = '<table style="width:50%">' +
        '<tr style="border: 1px solid black;"><th style="border: 1px solid black;">Tên Bên Mua</th><th style="border: 1px solid black;">Email</th><th style="border: 1px solid black;">Số điện thoại</th><th style="border: 1px solid black;">Tên Sản Phẩm</th><th style="border: 1px solid black;">Số lượng</th><th style="border: 1px solid black;">Id</th>'

    let htmlContent = ""

    htmlContent += '<tr>' +
        '<td style="border: 1px solid black; font-size: 1.2rem; text-align: center;">' + data.name + '</td>' +
        '<td style="border: 1px solid black; font-size: 1.2rem; text-align: center;">' + data.email + '</td>' +
        '<td style="border: 1px solid black; font-size: 1.2rem; text-align: center;">' + data.phone + '</td>' +
        '<td style="border: 1px solid black; font-size: 1.2rem; text-align: center;">' + data.product_name + '</td>' +
        '<td style="border: 1px solid black; font-size: 1.2rem; text-align: center;">' + data.quantity + '</td>' +
        '<td style="border: 1px solid black; font-size: 1.2rem; text-align: center;">' + data.product_id + '</td>' +
        '<tr>'

    const htmlResult = '<h1>Yêu cầu đặt mua hàng </h1>' + htmlHead + htmlContent
    // const ccList = ['anh.tran@beetech.com.vn'];
    // Thực hiện gửi email (to, subject, htmlContent)
    await mailer.sendMail("sale@beetech.com.vn", 'Thông tin mua hàng từ khách trên Barcodesolution web page', htmlResult)

    res.send("Gui Email Thanh Cong")

}

