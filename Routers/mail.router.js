const express = require('express')
const middleware = require('../Middlewares/rate_limit')

const router = express.Router()

const Order = require('../Controllers/mail.controller')

router.post('/email', middleware.apiLimiter, Order.send_mail)

module.exports = router