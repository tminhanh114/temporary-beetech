const express = require('express')
const app = express();
const cors = require("cors");
const upload = require('express-fileupload');
const morgan = require('morgan');
const bodyParser = require("body-parser");
const dotenv = require('dotenv').config();
const http = require('http').Server(app);


const EmailAPI = require('./Routers/mail.router')
// Setting up config file 
if (process.env.NODE_ENV !== 'PRODUCTION') require('dotenv').config({ path: '.env' })


app.use('/', express.static('public'))
app.use(upload());
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));
// app.use(cookieParser())
app.use(morgan(':method :url :status :response-time ms :date[iso]'))
app.use(bodyParser.json());
app.use(cors({
  origin: "*"
}));


app.use('/api/v1', EmailAPI)

http.listen(process.env.PORT, () => {
  console.log('listening on *: ' + process.env.PORT);
});

// Handle Unhandled Promise rejections
process.on('unhandledRejection', err => {
  console.log(`ERROR: ${err.stack}`);
  console.log('Shutting down the server due to Unhandled Promise rejection');
  server.close(() => {
    process.exit(1)
  });
});

// Handle Uncaught exceptions
process.on('uncaughtException', err => {
  console.log(`ERROR: ${err.stack}`);
  console.log('Shutting down due to uncaught exception');
  process.exit(1)
})

